-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 16 mai 2019 à 17:37
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_planing`
--

-- --------------------------------------------------------

--
-- Structure de la table `planing`
--

DROP TABLE IF EXISTS `planing`;
CREATE TABLE IF NOT EXISTS `planing` (
  `idPlaning` int(11) NOT NULL AUTO_INCREMENT,
  `debutSemaine` varchar(20) NOT NULL,
  `jour` varchar(20) NOT NULL,
  `validationDepart` varchar(100) NOT NULL,
  `heureDepart` varchar(20) NOT NULL,
  `heureRetour` varchar(20) NOT NULL,
  `ResponsableDepart` varchar(50) NOT NULL,
  `ResponsableRetour` varchar(50) NOT NULL,
  PRIMARY KEY (`idPlaning`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `planing`
--

INSERT INTO `planing` (`idPlaning`, `debutSemaine`, `jour`, `validationDepart`, `heureDepart`, `heureRetour`, `ResponsableDepart`, `ResponsableRetour`) VALUES
(98, '20/05/2019', 'LUNDI', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(99, '20/05/2019', 'MARDI', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(100, '20/05/2019', 'MERCREDI', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(101, '20/05/2019', 'JEUDI', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(102, '20/05/2019', 'VENDREDI', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel');

-- --------------------------------------------------------

--
-- Structure de la table `responsable`
--

DROP TABLE IF EXISTS `responsable`;
CREATE TABLE IF NOT EXISTS `responsable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `responsable`
--

INSERT INTO `responsable` (`id`, `nom`) VALUES
(1, 'Tanel'),
(2, 'Sonya');

-- --------------------------------------------------------

--
-- Structure de la table `tousplaning`
--

DROP TABLE IF EXISTS `tousplaning`;
CREATE TABLE IF NOT EXISTS `tousplaning` (
  `idPl` int(11) NOT NULL AUTO_INCREMENT,
  `jourPl` varchar(20) NOT NULL,
  `debutSem` varchar(20) NOT NULL,
  `validationDep` varchar(100) NOT NULL,
  `heureDep` varchar(20) NOT NULL,
  `heureRet` varchar(20) NOT NULL,
  `RespDep` varchar(50) NOT NULL,
  `RespRet` varchar(50) NOT NULL,
  PRIMARY KEY (`idPl`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tousplaning`
--

INSERT INTO `tousplaning` (`idPl`, `jourPl`, `debutSem`, `validationDep`, `heureDep`, `heureRet`, `RespDep`, `RespRet`) VALUES
(31, 'LUNDI', '13/05/2019', 'Reste Ã  la maison', '00:00', '00:00', 'NULL', 'NULL'),
(32, 'MARDI', '13/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(33, 'MERCREDI', '13/05/2019', 'Reste Ã  la maison', '00:00', '00:00', 'NULL', 'NULL'),
(34, 'JEUDI', '13/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(35, 'VENDREDI', '13/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(36, 'LUNDI', '20/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(37, 'MARDI', '20/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(38, 'MERCREDI', '20/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(39, 'JEUDI', '20/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel'),
(40, 'VENDREDI', '20/05/2019', 'part chez le nounou', '00:00', '00:00', 'Tanel', 'Tanel');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
