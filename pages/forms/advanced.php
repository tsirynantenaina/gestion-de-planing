<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestion Planing</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b>EB</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Appli</b>WEB</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><i class="fa fa-gears"></i></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">                 
                  <div style="width:100% ; text-align:center; ">
                    <h4>Suppression des donneess</h4>
                    <img src="../../Waste_96px.png" class="img-circle" alt="User Image">
                    
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                <script type="text/javascript">function msg5( ){
                        if(confirm("Votre deonnees est supprimer seulement de la page d'acceuil mais reste dans la page des listes! Confirmer la Suppression?")){
                          window.location.replace('../../delHome.php?page=3');
                        }
                        
                }</script>
                  <button  class="btn btn-default btn-flat" onclick="return msg5()">Cette semaine</button>
                </div>
               
                <div class="pull-right">
                 <script type="text/javascript">
                    function dropDonnees(){
                      if(confirm("Attention,tous les donnees enregistrées va etre suppriméés! Confirmer la Suppression?")){
                          window.location.replace('../../dropAll.php?page=3');
                        }
                    }
                                        </script>
                  <button onclick="return dropDonnees()" class="btn btn-default btn-flat" >Tous</button>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>

      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../Capture.PNG" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Gestion Planing</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU DE NAVIGATION</li>
        <li>
          <a href="../../index.php">
            <i class="fa fa-dashboard"></i> <span>ACCEUIL</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-edit"></i><span>NOUVEAU</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li> 
        <li>
          <a href="../tables/data.php">
            <i class="fa fa-table"></i> <span>TOUTES LES LISTES</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

          <li class="treeview">
          <a href="#">
            <i class="fa fa-circle-o"></i>
            <span>LES ACAMPAGNATS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"><br>
                   <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<li><span style="color:white ; margin-left:46px;">'.$donnees['nom'].'<a href="../../del.php?id='.$donnees['id'].'" style="margin-right:5px ; float:right ; margin-top:-20px; color:orange; "> supprimer </a></span>                                  
                            </li>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                <br>
          <form method="post" action="../../add.php">
             <li><div class="input-group input-group-sm">
                <input type="text" class="form-control" name="nom">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary">Ajouter</button>
                    </span>
          
                </div>
            </li>
          </form>
            <br>
          </ul>      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <FIELDSET>FORMULAIRE</FIELDSET>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>Page </a></li>
        <li class="active">Nouveau</li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <form method="post" action="post.php">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Semaine de</h3>
         <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="debutSemaine" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask required>
                </div>
          <div class="box-tools pull-right">
            <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <div class="box box-success"><!-- bordure vert--></div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Jour</label>
                <input type="text" value="LUNDI" name="jour1" class="form-control"   disabled="disabled" style="width: 100%;" required/>
              </div>
              <!-- /.form-group -->
              
                <div class="form-group">
                  <label>Heure de depart</label>
                  <div class="input-group">
                  <script type="text/javascript">
                  function control(){
                    var txt = document.getElementById("id1").value;
                    var data = document.getElementById("id1").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id1").value=phrase2;
                      }   
                    }

                    function control1(){
                        var txt = document.getElementById("id1").value;
                      var data = document.getElementById("id1").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id1").value=phrase;
                      }

                    }

                    function controlId2(){
                    var txt = document.getElementById("id2").value;
                    var data = document.getElementById("id2").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id2").value=phrase2;
                      }   
                    }

                    function controlerId2(){
                        var txt = document.getElementById("id2").value;
                      var data = document.getElementById("id2").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id2").value=phrase;
                      }

                    }

                    function controlId3(){
                    var txt = document.getElementById("id3").value;
                    var data = document.getElementById("id3").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id3").value=phrase2;
                      }   
                    }

                    function controlerId3(){
                        var txt = document.getElementById("id3").value;
                      var data = document.getElementById("id3").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id3").value=phrase;
                      }

                    }


                    function controlId4(){
                    var txt = document.getElementById("id4").value;
                    var data = document.getElementById("id4").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id4").value=phrase2;
                      }   
                    }

                    function controlerId4(){
                        var txt = document.getElementById("id4").value;
                      var data = document.getElementById("id4").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id4").value=phrase;
                      }

                    }

                    function controlId5(){
                    var txt = document.getElementById("id5").value;
                    var data = document.getElementById("id5").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id5").value=phrase2;
                      }   
                    }

                    function controlerId5(){
                        var txt = document.getElementById("id5").value;
                      var data = document.getElementById("id5").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id5").value=phrase;
                      }

                    }


function controlId6(){
                    var txt = document.getElementById("id6").value;
                    var data = document.getElementById("id6").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id6").value=phrase2;
                      }   
                    }

                    function controlerId6(){
                        var txt = document.getElementById("id6").value;
                      var data = document.getElementById("id6").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id6").value=phrase;
                      }

                    }

function controlId7(){
                    var txt = document.getElementById("id7").value;
                    var data = document.getElementById("id7").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id7").value=phrase2;
                      }   
                    }

                    function controlerId7(){
                        var txt = document.getElementById("id7").value;
                      var data = document.getElementById("id7").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id7").value=phrase;
                      }

                    }


function controlId8(){
                    var txt = document.getElementById("id8").value;
                    var data = document.getElementById("id8").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id8").value=phrase2;
                      }   
                    }

                    function controlerId8(){
                        var txt = document.getElementById("id8").value;
                      var data = document.getElementById("id8").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id8").value=phrase;
                      }

                    }



function controlId9(){
                    var txt = document.getElementById("id9").value;
                    var data = document.getElementById("id9").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id9").value=phrase2;
                      }   
                    }

                    function controlerId9(){
                        var txt = document.getElementById("id9").value;
                      var data = document.getElementById("id9").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id9").value=phrase;
                      }

                    }

                    function controlId10(){
                    var txt = document.getElementById("id10").value;
                    var data = document.getElementById("id10").value;
                    var exp=new RegExp("^[0-9:-: ]*$","g");
                    var nb = txt.length;
                    var res =exp.test(data);    
                      if (!res) {
                        alert("l'heure doit etre de la forme 00:00"); 
                        var phrase=txt.substring(0,nb)
                        var phrase2=txt.substring(0,nb-1);
                        document.getElementById("id10").value=phrase2;
                      }   
                    }

                    function controlerId10(){
                        var txt = document.getElementById("id10").value;
                      var data = document.getElementById("id10").value;
                      var exp=new RegExp("^[0-9:-: ]*$","g");
                      var nb = txt.length;
                      var res =exp.test(data);
                      if(txt=="00:00"){
                          var phrase=txt.substring(0,nb-5);
                          document.getElementById("id10").value=phrase;
                      }

                    }

                </script>

                    <input type="text" name="heureDepart1" id="id1" onkeyup="control()" onclick="control1()" value="00:00" class="form-control" placeholder="00:00" required>
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui l'accampagne chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="mpanatitra1" required>
                      <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>

                  <option value="NULL">NULL (il reste à la maison)</option>

                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Le lundi , Nathan part-il chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="validationDepart1" required>    
                  <option>part chez le nounou</option>
                  <option>Reste à la maison</option>
                </select>
              </div>
              <!-- /.form-group -->
            
                <div class="form-group">
                  <label>Heure de retour</label>

                  <div class="input-group">
                    <input type="text" class="form-control" value="00:00" id="id2"  onkeyup="controlId2()" onclick="controlerId2()" placeholder="00:00" name="heureRetour1" required/>

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui le ramene à la maison?</label>
                <select class="form-control"name="mpitsena1" style="width: 100%;" required>
                    <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>
                </select>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box box-success"> <!-- bordure vert--> </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Jour</label>
                <input type="text" value="MARDI" name="jour2" class="form-control"   disabled="disabled" style="width: 100%;" required/>
              </div>
              <!-- /.form-group -->
              
                <div class="form-group">
                  <label>Heure de depart</label>
                  <div class="input-group">
                    <input type="text" name="heureDepart2" value="00:00" id="id3" onkeyup="controlId3()" onclick="controlerId3()" class="form-control" placeholder="00:00" required>
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui l'accampagne chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="mpanatitra2" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>

                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Le mardi , Nathan part-il chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="validationDepart2" required>    
                  <option>part chez le nounou</option>
                  <option>Reste à la maison</option>
                </select>
              </div>
              <!-- /.form-group -->
            
                <div class="form-group">
                  <label>Heure de retour</label>

                  <div class="input-group">
                    <input type="text" class="form-control" value="00:00" id="id4"  onkeyup="controlId4()" onclick="controlerId4()" placeholder="00:00" name="heureRetour2" required/>

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui le ramene à la maison? </label>
                <select class="form-control"name="mpitsena2" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>
                </select>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box box-success"> <!-- bordure vert--> </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Jour</label>
                <input type="text" value="MERCREDI" name="jour3" class="form-control"   disabled="disabled" style="width: 100%;" required/>
              </div>
              <!-- /.form-group -->
              
                <div class="form-group">
                  <label>Heure de depart</label>
                  <div class="input-group">
                    <input type="text" name="heureDepart3" value="00:00" id="id5"  onkeyup="controlId5()" onclick="controlerId5()" class="form-control" placeholder="00:00" required >
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui l'accampagne chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="mpanatitra3" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>

                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Le mercredi, Nathan part-il chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="validationDepart3" required>    
                  <option>part chez le nounou</option>
                  <option>Reste à la maison</option>
                </select>
              </div>
              <!-- /.form-group -->
            
                <div class="form-group">
                  <label>Heure de retour</label>

                  <div class="input-group">
                    <input type="text" class="form-control" value="00:00" id="id6"  onkeyup="controlId6()" onclick="controlerId6()" placeholder="00:00" name="heureRetour3" required/>

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui le ramene à la maison?</label>
                <select class="form-control"name="mpitsena3" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>
                </select>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box box-success"> <!-- bordure vert--> </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Jour</label>
                <input type="text" value="JEUDI" name="jour4" class="form-control"   disabled="disabled" style="width: 100%;" required/>
              </div>
              <!-- /.form-group -->
              
                <div class="form-group">
                  <label>Heure de depart</label>
                  <div class="input-group">
                    <input type="text" name="heureDepart4" value="00:00" id="id7"  onkeyup="controlId7()" onclick="controlerId7()" class="form-control" placeholder="00:00" required>
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui l'accampagne chez le nounou?</label>
                <select name="mpanatitra4"  class="form-control" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>

                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>le jeudi , Nathan part-il chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="validationDepart4" required>    
                  <option>part chez le nounou</option>
                  <option>Reste à la maison</option>
                </select>
              </div>
              <!-- /.form-group -->
            
                <div class="form-group">
                  <label>Heure de retour</label>

                  <div class="input-group">
                    <input type="text" class="form-control" value="00:00" id="id8"  onkeyup="controlId8()" onclick="controlerId8()"placeholder="00:00" name="heureRetour4" required/>

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui le ramene à la maison?</label>
                <select class="form-control"name="mpitsena4" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>
                </select>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box box-success"> <!-- bordure vert--> </div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Jour</label>
                <input type="text" value="VENDREDI" name="jour5" class="form-control"   disabled="disabled" style="width: 100%;" required/>
              </div>
              <!-- /.form-group -->
              
                <div class="form-group">
                  <label>Heure de depart</label>
                  <div class="input-group">
                    <input type="text" name="heureDepart5" value="00:00" id="id9"  onkeyup="controlId9()" onclick="controlerId9()"class="form-control" placeholder="00:00" required>
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui l'accampagne chez le nounou</label>
                <select name="mpanatitra5" class="form-control" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>

                </select>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Le vendredi , Nathan part-il chez le nounou?</label>
                <select class="form-control" style="width: 100%;" name="validationDepart5" required>    
                  <option>part chez le nounou</option>
                  <option>Reste à la maison</option>
                </select>
              </div>
              <!-- /.form-group -->
            
                <div class="form-group">
                  <label>Heure de retour</label>

                  <div class="input-group">
                    <input type="text" class="form-control" value="00:00" id="id10"  onkeyup="controlId10()" onclick="controlerId10()" placeholder="00:00" name="heureRetour5" required/>

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              
              <!-- /.form-group -->
              <div class="form-group">           
                <label>Qui le ramene à la maison?</label> 
                <select class="form-control" name="mpitsena5" style="width: 100%;" required>
                     <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<option>'.$donnees['nom'].'</option>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                  <option value="NULL">NULL (il reste à la maison)</option>
                </select>
              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box box-success"> <!-- bordure vert--> </div>


        <div class="box-footer">
          <input type="submit" value="Sauvegarder" style="width:150px; float:right;" class="btn btn-primary btn-block margin-bottom" />
        </div>
      </div>
      <br><br><br><br><br><br><br><br><br><br><br><br>
  </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b><a>ratsirynantenainajoathan@gmail.com</a></b> 
    </div>
    <strong>Dev By Tsiry Nantenaina </strong>
  </footer>


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
