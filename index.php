<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestion Planing</title>
  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <script type="text/javascript">
     function printContent(el){
      var restorepage=document.body.innerHTML;
      var printContent=document.getElementById(el).innerHTML;
      document.body.innerHTML=printContent;
      window.print();
      document.body.innerHTML=restorepage;
    }
  </script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b>EB</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Appli</b>WEB</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><i class="fa fa-gears"></i></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">                 
                  <div style="width:100% ; text-align:center; ">
                    <h4>Suppression des donneess</h4>
                    <img src="Waste_96px.png" class="img-circle" alt="User Image">
                    
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                <script type="text/javascript">function msg5( ){
                        if(confirm("Votre deonnees est supprimer seulement de la page d'acceuil mais reste dans la page des listes! Confirmer la Suppression?")){
                          window.location.replace('delHome.php?page=1');
                        }
                        
                }</script>
                  <button  class="btn btn-default btn-flat" onclick="return msg5()">Cette semaine</button>
                </div>
               
                <div class="pull-right">
                 <script type="text/javascript">
                    function dropDonnees(){
                      if(confirm("Attention,tous les donnees enregistrées va etre suppriméés! Confirmer la Suppression?")){
                          window.location.replace('dropAll.php?page=1');
                        }
                    }
                                        </script>
                  <button onclick="return dropDonnees()" class="btn btn-default btn-flat" >Tous</button>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="Capture.PNG" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Gestion de Planing</p>
          
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU DE NAVIGATION</li>
        
        <li>
          <a href="#" >
            <i class="fa fa-dashboard" ></i> <span>ACCEUILL</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li>
          <a href="pages/forms/advanced.php">
            <i class="fa fa-edit"></i><span>NOUVEAU</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li> 
        <li>
          <a href="pages/tables/data.php">
            <i class="fa fa-table"></i> <span>TOUTES LES LISTES</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-circle-o"></i>
            <span>LES ACAMPAGNATS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"><br>
                   <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT* FROM responsable ORDER BY id ASC');
                        while($donnees=$req->fetch()){
                            echo'<li><span style="color:white ; margin-left:46px;">'.$donnees['nom'].'<a href="del.php?id='.$donnees['id'].'" style="margin-right:5px ; float:right ; margin-top:-20px; color:orange; "> supprimer </a></span>                                  
                            </li>';     
                            }
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
                <br>
          <form method="post" action="add.php">
             <li><div class="input-group input-group-sm">
                <input type="text" class="form-control" name="nom">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary">Ajouter</button>
                    </span>
          
                </div>
            </li>
          </form>
            <br>
          </ul>

      
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ACCEUIL
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i>Page</a></li>
        <li class="active">Acceuil</li>
      </ol>
    </section><br>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT count(*) as total from tousPlaning');
                    $donnees=$req->fetch();
                     $val=$donnees['total'];
                    if($val>1){
                      echo'<h3>
                                    '.$donnees['total'].' lignes                               
                                </h3>';
                    }else{
                      echo'<h3>
                                    '.$donnees['total'].' ligne                              
                                </h3>';
                    }               
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
              <p>Total liste d'enregistrement</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="pages/tables/data.php" class="small-box-footer">Plus d'Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT count(*) as total from planing WHERE validationDepart="part chez le nounou" ORDER BY idPlaning DESC LIMIT 5');
                    $donnees=$req->fetch();
                     $val=$donnees['total'];
                    if($val>1){
                      echo'<h3>
                                    '.$donnees['total'].' jours                               
                                </h3>';
                    }else{
                      echo'<h3>
                                    '.$donnees['total'].' jour                              
                                </h3>';
                    }    
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }
                            
                    $req->closeCursor();
                ?>
              <p>Chez le nounou cette semaine</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
              <script type="text/javascript"> function msg(){alert("Verifier sur le tableau ci-dessous"); }</script>
            </div >
            
            <a href="#" class="small-box-footer" onclick="msg()">Plus d'Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT count(*) as total from planing WHERE validationDepart="Reste à la maison" ORDER BY idPlaning DESC LIMIT 5');
                    $donnees=$req->fetch();
                    $val=$donnees['total'];
                    if($val>1){
                      echo'<h3>
                                    '.$donnees['total'].' jours                               
                                </h3>';
                    }else{
                      echo'<h3>
                                    '.$donnees['total'].' jour                              
                                </h3>';
                    }
                             
                        
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }         
                    $req->closeCursor();
                ?>

              <p>reste à la maison cette semaine</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer" onclick="msg()" >Plus d'Info<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
             <?php
                 try{
                    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                    $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                    $req=$bdd->query('SELECT count(*) as total from responsable');
                    $donnees=$req->fetch();
                    $val=$donnees['total'];
                    if($val<1){
                      echo'<h3>
                                    '.$donnees['total'].' personnes                               
                                </h3>';
                    }else{
                      echo'<h3>
                                    '.$donnees['total'].' personnes                             
                                </h3>';
                    }
                             
                        
                 }catch(Exception $e){
                    die('ERREUR'.$e->getMessage());
                 }         
                    $req->closeCursor();
                ?>
            
              <p>Acampagnants</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <script type="text/javascript"> function msg2(){alert("Veuillez naviger sur le menu 'LES ACAMPAGNATS' "); }</script>
            <a href="#s" class="small-box-footer" onclick="msg2()">Plus d'Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
       <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3>Planing de cette semaine                 
                    <button onclick="printContent('imprim')" class="btn btn-block btn-default" style="width:100px; float:right ; ">Imprimer</button>
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <div id="imprim">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Semaine de</th>
                          <th>Jour</th>
                          <th>Confirmation De depart</th>
                          <th>Heure de Depart</th>
                          <th>Heure de rentré</th>
                          <th>Accampagnant aller</th>
                          <th>Accampagnant retour</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                         try{
                            $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                            $bdd= new PDO('mysql: host=localhost ; dbname=gestion_planing','root','',$pdo_options);
                            $req=$bdd->query('SELECT* FROM planing ORDER BY idPlaning ASC');
                                while($donnees=$req->fetch()){
                                    echo'<tr>
                                            <td>'.$donnees['debutSemaine'].'</td>                               
                                            <td>'.$donnees['jour'].'</td>
                                            <td>'.$donnees['validationDepart'].'</td>
                                            <td>'.$donnees['heureDepart'].'</td>
                                            <td>'.$donnees['heureRetour'].'</td>
                                            <td>'.$donnees['ResponsableDepart'].'</td>
                                            <td>'.$donnees['ResponsableRetour'].'</td>
                                            <td><a class="" href="pages/forms/edit.php?idPl='.$donnees['idPlaning'].'&semaine='.$donnees['debutSemaine'].'&jour='.$donnees['jour'].'&validation='.$donnees['validationDepart'].'&heureDepart='.$donnees['heureDepart'].'&heureRetour='.$donnees['heureRetour'].'&ResponsableDepart='.$donnees['ResponsableDepart'].'&ResponsableRetour='.$donnees['ResponsableRetour'].'"><i class="fa fa-edit"></i> Edit</a></a>

                                            </td>
                                        </tr>';     
                                    }
                         }catch(Exception $e){
                            die('ERREUR'.$e->getMessage());
                         }
                                    
                            $req->closeCursor();
                        ?>
                        
                        </tbody>
                       
                      </table><br>
                  </div>
            </div>
            <!-- /.box-body --><div class="box"></div>
          </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <br><br><br><br><br><br><br><br><br> <br><br><br><br><br><br><br><br><br>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b><a>ratsirynantenainajoathan@gmail.com</a></b> 
    </div>
    <strong>Dev By Tsiry Nantenaina </strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
